#include <iostream>
#include <string>

int main()
{
	std::string TestString("UnrealEngine");
	std::cout << TestString << '\n';
	std::cout << TestString.length() << '\n';
	std::cout << TestString.substr(0, 1) << '\n';
	std::cout << TestString.substr(TestString.length() - 1, 1) << '\n';
}